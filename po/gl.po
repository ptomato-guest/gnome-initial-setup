# Galician translations for gnome-initial-setup package.
# Copyright (C) 2012 Fran Diéguez
# This file is distributed under the same license as the gnome-initial-setup package.
# Proxecto Trasno - Adaptación do software libre á lingua galega:  Se desexas
# colaborar connosco, podes atopar máis información en http://www.trasno.net
# Leandro Regueiro <leandro.regueiro@gmail.com>, 2012.
# Fran Dieguez <frandieguez@gnome.org>, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: gnome-initial-setup\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-initial-setup/"
"issues\n"
"POT-Creation-Date: 2019-02-07 08:37+0000\n"
"PO-Revision-Date: 2019-02-07 17:54+0200\n"
"Last-Translator: Fran Dieguez <frandieguez@gnome.org>\n"
"Language-Team: Galician\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.7.1\n"
"X-Project-Style: gnome\n"

#: data/gnome-initial-setup.desktop.in.in:3
#: data/gnome-initial-setup-first-login.desktop.in.in:3
msgid "Initial Setup"
msgstr "Configuración inicialsh"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: data/gnome-initial-setup.desktop.in.in:5
#: data/gnome-initial-setup-first-login.desktop.in.in:5
msgid "preferences-system"
msgstr "preferences-system"

#: gnome-initial-setup/gis-assistant.c:423
msgid "_Next"
msgstr "Segui_nte"

#: gnome-initial-setup/gis-assistant.c:424
msgid "_Accept"
msgstr "_Aceptar"

#: gnome-initial-setup/gis-assistant.c:425
msgid "_Skip"
msgstr "_Omitir"

#: gnome-initial-setup/gis-assistant.c:426
msgid "_Previous"
msgstr "_Anterior"

#: gnome-initial-setup/gis-assistant.c:427
msgid "_Cancel"
msgstr "_Cancelar"

#: gnome-initial-setup/gnome-initial-setup.c:259
msgid "Force existing user mode"
msgstr "Forzar o modo de usuario existente"

#: gnome-initial-setup/gnome-initial-setup.c:265
msgid "— GNOME initial setup"
msgstr "— Configuración inicial de GNOME"

#: gnome-initial-setup/pages/account/gis-account-avatar-chooser.ui:36
msgid "Take a Picture…"
msgstr "Sacar unha foto…"

#: gnome-initial-setup/pages/account/gis-account-page.c:264
#: gnome-initial-setup/pages/account/gis-account-page-local.ui:41
msgid "About You"
msgstr "Sobre vostede"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:182
msgid "Failed to register account"
msgstr "Produciuse un erro ao rexistrar a conta"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:376
msgid "No supported way to authenticate with this domain"
msgstr "Non hai unha maneira admitida de autenticar con este dominio"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:416
msgid "Failed to join domain"
msgstr "Produciuse un erro ao unirse ao dominio"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:484
msgid "Failed to log into domain"
msgstr "Produciuse un erro ao iniciar sesión no dominio"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:31
msgid "Enterprise Login"
msgstr "Inicio de sesión corporativo"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:45
msgid ""
"Enterprise login allows an existing centrally managed user account to be "
"used on this device. You can also use this account to access company "
"resources on the internet."
msgstr ""
"O inicio de sesión corporativo permítelle xestionar unha conta de usuario "
"xestionada de forma centralizada para usala neste dispositivo. Tamén pode "
"usar esta conta para acceder aos recursos da empresa en internet."

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:65
#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:316
msgid "_Domain"
msgstr "_Dominio"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:82
#: gnome-initial-setup/pages/account/gis-account-page-local.ui:101
msgid "_Username"
msgstr "_Nome de usuario"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:99
#: gnome-initial-setup/pages/password/gis-password-page.ui:66
msgid "_Password"
msgstr "_Contrasinal"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:175
msgid "Enterprise domain or realm name"
msgstr "Dominio da empresa ou nome do reino"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:240
msgid "C_ontinue"
msgstr "C_ontinuar"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:275
msgid "Domain Administrator Login"
msgstr "Inicio de sesión do administrador do dominio"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:295
msgid ""
"In order to use enterprise logins, this computer needs to be enrolled in a "
"domain. Please have your network administrator type the domain password "
"here, and choose a unique computer name for your computer."
msgstr ""
"Para usar inicio de sesión corporativo, este equipo precisa formar parte dun "
"dominio. Pida ao administrador do seu sistema que escriba aquí o contrasinal "
"do dominio, e seleccione un nome para o seu computador."

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:350
msgid "_Computer"
msgstr "_Computador"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:382
msgid "Administrator _Name"
msgstr "_Nome do administrador"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:416
msgid "Administrator Password"
msgstr "Contrasinal do administrador"

#: gnome-initial-setup/pages/account/gis-account-page-local.c:203
msgid "Please check the name and username. You can choose a picture too."
msgstr "Comprobe o nome e nome de usuario. Tamén pode escoller unha imaxe."

#: gnome-initial-setup/pages/account/gis-account-page-local.c:422
msgid "We need a few details to complete setup."
msgstr "Precisamos algunha información para completar a configuración."

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:21
msgid "Avatar image"
msgstr "Imaxe do avatar"

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:53
msgid "Please provide a name and username. You can choose a picture too."
msgstr "Forneza o nome e nome de usuario. Tamén pode escoller unha imaxe."

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:70
msgid "_Full Name"
msgstr "_Nome completo"

#: gnome-initial-setup/pages/account/gis-account-page.ui:43
#| msgid "Enterprise Login"
msgid "_Enterprise Login"
msgstr "Inicio de sesión _corporativo"

#: gnome-initial-setup/pages/account/gis-account-page.ui:53
#| msgid "Set Up _Enterprise Login"
msgid "Go online to set up Enterprise Login."
msgstr "Conectarse para configurar o Inicio de sesión _corporativo"

#: gnome-initial-setup/pages/account/um-realm-manager.c:310
msgid "Cannot automatically join this type of domain"
msgstr "Non é posíbel unirse automaticamente a este tipo de dominio"

#: gnome-initial-setup/pages/account/um-realm-manager.c:373
#, c-format
msgid "No such domain or realm found"
msgstr "Non existe o dominio ou non se atopou o reino"

#: gnome-initial-setup/pages/account/um-realm-manager.c:782
#: gnome-initial-setup/pages/account/um-realm-manager.c:796
#, c-format
msgid "Cannot log in as %s at the %s domain"
msgstr "Non foi posíbel iniciar sesión como %s no dominio %s"

#: gnome-initial-setup/pages/account/um-realm-manager.c:788
msgid "Invalid password, please try again"
msgstr "Contrasinal incorrecto, ténteo de novo"

#: gnome-initial-setup/pages/account/um-realm-manager.c:801
#, c-format
msgid "Couldn’t connect to the %s domain: %s"
msgstr "Non foi posíbel conectarse ao dominio %s: %s"

#: gnome-initial-setup/pages/account/um-utils.c:245
msgid "Sorry, that user name isn’t available. Please try another."
msgstr "O nome de usuario non está dispoñíbel. Probe outro."

#: gnome-initial-setup/pages/account/um-utils.c:248
#, c-format
msgid "The username is too long."
msgstr "O nome de usuario é demasiado longo."

#: gnome-initial-setup/pages/account/um-utils.c:251
msgid "The username cannot start with a “-”."
msgstr "O nome de usuario non pode comezar con «-»."

#: gnome-initial-setup/pages/account/um-utils.c:254
msgid ""
"The username should only consist of upper and lower case letters from a-z, "
"digits and the following characters: . - _"
msgstr ""
"O nome de usuario debe estar formado por letras maiúsculas e minúśculas do "
"«a» ao «z», díxitos e os caracteres «.», «-» e «_»."

#: gnome-initial-setup/pages/account/um-utils.c:258
msgid "This will be used to name your home folder and can’t be changed."
msgstr "Usarase como nome para o seu cartafol persoal e non pode cambiarse."

#: gnome-initial-setup/pages/eulas/gis-eula-page.c:301
#: gnome-initial-setup/pages/eulas/gis-eula-page.ui:21
msgid "License Agreements"
msgstr "Acordos de licenza"

#: gnome-initial-setup/pages/eulas/gis-eula-page.ui:50
msgid ""
"I have _agreed to the terms and conditions in this end user license "
"agreement."
msgstr "_Acepto os termos e condicións deste acordo de licenza de usuario."

#: gnome-initial-setup/pages/goa/gis-goa-page.c:87
msgid "Add Account"
msgstr "Engadir conta"

#: gnome-initial-setup/pages/goa/gis-goa-page.c:339
msgid "Online Accounts"
msgstr "Contas en liña"

#: gnome-initial-setup/pages/goa/gis-goa-page.ui:39
msgid "Connect Your Online Accounts"
msgstr "Conecte as súas contas en liña"

#: gnome-initial-setup/pages/goa/gis-goa-page.ui:58
msgid ""
"Connect your accounts to easily access your email, online calendar, "
"contacts, documents and photos."
msgstr ""
"Conecte as súas contas para acceder de forma doada ao seu correo, calendario "
"en liña, contactos, documentos e fotos."

#: gnome-initial-setup/pages/goa/gis-goa-page.ui:95
msgid ""
"Accounts can be added and removed at any time from the Settings application."
msgstr ""
"Pode engadir e quitar contas en calquera momento desde o aplicativo "
"Preferencias."

#: gnome-initial-setup/pages/keyboard/cc-input-chooser.c:240
msgid "Preview"
msgstr "Previsualizar"

#: gnome-initial-setup/pages/keyboard/cc-input-chooser.c:299
#: gnome-initial-setup/pages/language/cc-language-chooser.c:222
#: gnome-initial-setup/pages/region/cc-region-chooser.c:210
msgid "More…"
msgstr "Máis…"

#. Translators: a search for input methods or keyboard layouts
#. * did not yield any results
#.
#: gnome-initial-setup/pages/keyboard/cc-input-chooser.c:321
msgid "No inputs found"
msgstr "Non se atopou ningunha orixe de entrada"

#: gnome-initial-setup/pages/keyboard/gis-keyboard-page.c:480
#: gnome-initial-setup/pages/keyboard/gis-keyboard-page.ui:30
msgid "Typing"
msgstr "Escribindo"

#: gnome-initial-setup/pages/keyboard/gis-keyboard-page.ui:42
msgid "Select your keyboard layout or an input method."
msgstr "Seleccione a súa disposición de teclado ou un método de entrada."

#: gnome-initial-setup/pages/language/cc-language-chooser.c:238
msgid "No languages found"
msgstr "Non se atoparon idiomas"

#: gnome-initial-setup/pages/language/gis-language-page.c:308
msgid "Welcome"
msgstr "Benvido/a"

#. Translators: This is meant to be a warm, engaging welcome message,
#. * like greeting somebody at the door. If the exclamation mark is not
#. * suitable for this in your language you may replace it.
#.
#: gnome-initial-setup/pages/language/gis-welcome-widget.c:123
msgid "Welcome!"
msgstr "Benvida!"

#: gnome-initial-setup/pages/network/gis-network-page.c:313
msgctxt "Wireless access point"
msgid "Other…"
msgstr "Outro…"

#: gnome-initial-setup/pages/network/gis-network-page.c:398
msgid "Wireless networking is disabled"
msgstr "A rede sen fíos está desactivada"

#: gnome-initial-setup/pages/network/gis-network-page.c:405
msgid "Checking for available wireless networks"
msgstr "Buscando redes sen fíos dispoñíbeis"

#: gnome-initial-setup/pages/network/gis-network-page.c:713
msgid "Network"
msgstr "Rede"

#: gnome-initial-setup/pages/network/gis-network-page.ui:33
msgid "Wi-Fi"
msgstr "Wifi"

#: gnome-initial-setup/pages/network/gis-network-page.ui:45
msgid ""
"Connecting to the Internet will enable you to set the time, add your "
"details, and enable you to access your email, calendar, and contacts. It is "
"also necessary for enterprise login accounts."
msgstr ""
"Ao conectarse a Internet poderá estabelecer a súa hora, engadir a súa "
"información e permítelle acceder ao seu correo, calendario e contactos. "
"Tamén é necesario para iniciar sesión en contas corporativas."

#: gnome-initial-setup/pages/network/gis-network-page.ui:103
msgid "No wireless available"
msgstr "Rede sen fíos non dispoñíbel."

#: gnome-initial-setup/pages/network/gis-network-page.ui:118
msgid "Turn On"
msgstr "Activar"

#: gnome-initial-setup/pages/password/gis-password-page.c:142
msgid "This is a weak password."
msgstr "Este é un contrasinal débil."

#: gnome-initial-setup/pages/password/gis-password-page.c:148
msgid "The passwords do not match."
msgstr "Os contrasinais non coinciden."

#: gnome-initial-setup/pages/password/gis-password-page.c:270
msgid "Password"
msgstr "Contrasinal"

#: gnome-initial-setup/pages/password/gis-password-page.ui:34
msgid "Set a Password"
msgstr "Establecer un contrasinal"

#: gnome-initial-setup/pages/password/gis-password-page.ui:47
msgid "Be careful not to lose your password."
msgstr "Teña coidado e non perda o seu contrasinal."

#: gnome-initial-setup/pages/password/gis-password-page.ui:98
msgid "_Confirm"
msgstr "_Confirmar"

#: gnome-initial-setup/pages/password/pw-utils.c:81
msgctxt "Password hint"
msgid "The new password needs to be different from the old one."
msgstr "O novo contrasinal debe ser distinto ao antigo."

#: gnome-initial-setup/pages/password/pw-utils.c:83
msgctxt "Password hint"
msgid ""
"This password is very similar to your last one. Try changing some letters "
"and numbers."
msgstr ""
"Este contrasinal é moi similar ao seu último contrasinal. Tente cambiar "
"algunhas letras e números."

#: gnome-initial-setup/pages/password/pw-utils.c:85
#: gnome-initial-setup/pages/password/pw-utils.c:93
msgctxt "Password hint"
msgid ""
"This password is very similar to your last one. Try changing the password a "
"bit more."
msgstr ""
"Este contrasinal é moi similar ao seu último contrasinal. Tente cambiarlo "
"algo máis."

#: gnome-initial-setup/pages/password/pw-utils.c:87
msgctxt "Password hint"
msgid ""
"This is a weak password. A password without your user name would be stronger."
msgstr ""
"Este é un contrasinal débil. Un contrasinal sen o seu nome de usuario será "
"máis forte."

#: gnome-initial-setup/pages/password/pw-utils.c:89
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid using your name in the password."
msgstr "Este é un contrasinal débil. Tente evitar o uso do seu nome nel."

#: gnome-initial-setup/pages/password/pw-utils.c:91
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to avoid some of the words included in the "
"password."
msgstr ""
"Este é un contrasinal débil. Tente evitar algunhas palabras incluidas no "
"contrasinal."

#: gnome-initial-setup/pages/password/pw-utils.c:95
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid common words."
msgstr "Este é un contrasinal débil. Tente evitar palabras comúns."

#: gnome-initial-setup/pages/password/pw-utils.c:97
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid reordering existing words."
msgstr ""
"Este é un contrasinal débil. Tente evitar reordenar palabras existentes."

#: gnome-initial-setup/pages/password/pw-utils.c:99
msgctxt "Password hint"
msgid "This is a weak password. Try to use more numbers."
msgstr "Este é un contrasinal débil. Tente usar máis números."

#: gnome-initial-setup/pages/password/pw-utils.c:101
msgctxt "Password hint"
msgid "This is a weak password. Try to use more uppercase letters."
msgstr "Este é un contrasinal débil. Tente usar máis letras en maiúsculas."

#: gnome-initial-setup/pages/password/pw-utils.c:103
msgctxt "Password hint"
msgid "This is a weak password. Try to use more lowercase letters."
msgstr "Este é un contrasinal débil. Tente máis letras en minúsculas."

#: gnome-initial-setup/pages/password/pw-utils.c:105
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to use more special characters, like "
"punctuation."
msgstr ""
"Este é un contrasinal débil. Tente usar máis caracteres especiais, como "
"puntuación."

#: gnome-initial-setup/pages/password/pw-utils.c:107
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to use a mixture of letters, numbers and "
"punctuation."
msgstr ""
"Este é un contrasinal débil. Tente usar unha mistura de letras, números e "
"puntuación."

#: gnome-initial-setup/pages/password/pw-utils.c:109
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid repeating the same character."
msgstr "Este é un contrasinal débil. Tente evitar repetir o mesmo caracter."

#: gnome-initial-setup/pages/password/pw-utils.c:111
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to avoid repeating the same type of character: "
"you need to mix up letters, numbers and punctuation."
msgstr ""
"Este é un contrasinal débil. Tente evitar repetir o mesmo tipo de caracter: "
"debe misturar letras, números e puntuación."

#: gnome-initial-setup/pages/password/pw-utils.c:113
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid sequences like 1234 or abcd."
msgstr "Este é un contrasinal débil. Tente evitar secuencias como 1234 e abcd."

#: gnome-initial-setup/pages/password/pw-utils.c:115
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to add more letters, numbers and punctuation."
msgstr ""
"Este é un contrasinal débil. Tente engadir máis letras, números e símbolos."

#: gnome-initial-setup/pages/password/pw-utils.c:117
msgctxt "Password hint"
msgid "Mix uppercase and lowercase and try to use a number or two."
msgstr "Mesture letras en maiúsculas e minúsculas e use un número ou dous."

#: gnome-initial-setup/pages/password/pw-utils.c:119
msgctxt "Password hint"
msgid ""
"Adding more letters, numbers and punctuation will make the password stronger."
msgstr ""
"Ao engadir máis letras, números e signos de puntuación será máis forte."

#. Translators: the parameter here is the name of a distribution,
#. * like "Fedora" or "Ubuntu". It falls back to "GNOME" if we can't
#. * detect any distribution.
#.
#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:110
#, c-format
msgid ""
"Sending reports of technical problems helps us to improve %s. Reports are "
"sent anonymously and are scrubbed of personal data."
msgstr ""
"Enviando informes de problemas técnicos axúdanos a mellorar %s. Os informes "
"envíanse de forma anónima e son limpados de datos persoais."

#. Translators: the parameter here is the name of a distribution,
#. * like "Fedora" or "Ubuntu". It falls back to "GNOME" if we can't
#. * detect any distribution.
#.
#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:120
#, c-format
msgid "Problem data will be collected by %s:"
msgstr "Os datos do problema serán recollidos por %s:"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:121
#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:181
#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:256
msgid "Privacy Policy"
msgstr "Política de privacidade"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:181
msgid "Uses Mozilla Location Service:"
msgstr "Usar o servizo de localización de Mozilla:"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:292
#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:32
msgid "Privacy"
msgstr "Privacidade"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:49
msgid "Location Services"
msgstr "Servizos de localización"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:70
msgid ""
"Allows applications to determine your geographical location. An indication "
"is shown when location services are in use."
msgstr ""
"Permítelle aos aplicativos determinar a súa localización xeográfica. "
"Aparecerá unha indicación cada vez que se use o servizo de localización."

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:92
msgid "Automatic Problem Reporting"
msgstr "Informe de problemas automático"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:128
msgid ""
"Privacy controls can be changed at any time from the Settings application."
msgstr ""
"Os controis de Privacidade poden cambiarse en calquera momento desde o "
"aplicativo de Preferencias."

#: gnome-initial-setup/pages/region/cc-region-chooser.c:227
msgid "No regions found"
msgstr "Non se atoparon rexións"

#: gnome-initial-setup/pages/region/gis-region-page.c:226
#: gnome-initial-setup/pages/region/gis-region-page.ui:31
msgid "Region"
msgstr "Rexión"

#: gnome-initial-setup/pages/region/gis-region-page.ui:44
msgid "Choose your country or region."
msgstr "Seleccione o seu país ou rexión."

#: gnome-initial-setup/pages/software/gis-software-page.c:184
msgid "Software Repositories"
msgstr "Repositorios de software"

#. TRANSLATORS: this is the third party repositories info bar.
#: gnome-initial-setup/pages/software/gis-software-page.c:188
msgid "Access additional software from selected third party sources."
msgstr ""
"Acceder a software adicional desde as orixes de terceiros seleccionadas."

#. TRANSLATORS: this is the third party repositories info bar.
#: gnome-initial-setup/pages/software/gis-software-page.c:192
msgid ""
"Some of this software is proprietary and therefore has restrictions on use, "
"sharing, and access to source code."
msgstr ""
"Algún deste software é propietario e polo tanto ten restricións de uso, "
"compartición e acceso ao código fonte."

#: gnome-initial-setup/pages/software/gis-software-page.ui:31
msgid "Additional Software Repositories"
msgstr "Orixes de software adicional"

#: gnome-initial-setup/pages/software/gis-software-page.ui:55
msgid "<a href=\"more\">Find out more…</a>"
msgstr "<a href=\"more\">Saber máis…</a>"

#: gnome-initial-setup/pages/software/gis-software-page.ui:69
msgid "Third Party Repositories"
msgstr "Repositorios de terceiros"

#: gnome-initial-setup/pages/software/gis-software-page.ui:165
msgid ""
"Proprietary software typically has restrictions on how it can be used and on "
"access to source code. This prevents anyone but the software owner from "
"inspecting, improving or learning from its code."
msgstr ""
"O software privativo normalmente ten restricións sobre como pode ser usado e "
"no acceso ao código fonte. Isto evita que calquera salvo o propietario do "
"software poida inspeccionar, mellorar ou aprender do seu código."

#: gnome-initial-setup/pages/software/gis-software-page.ui:177
msgid ""
"In contrast, Free Software can be freely run, copied, distributed, studied "
"and modified."
msgstr ""
"Pola contra, o software libre pode executarse, copiarse, distribuírse, "
"estudarse e modificarse libremente."

#. Translators: the parameter here is the name of a distribution,
#. * like "Fedora" or "Ubuntu". It falls back to "GNOME 3" if we can't
#. * detect any distribution.
#: gnome-initial-setup/pages/summary/gis-summary-page.c:304
#, c-format
msgid "_Start Using %s"
msgstr "_Comezar a usar %s"

#: gnome-initial-setup/pages/summary/gis-summary-page.c:334
msgid "Ready to Go"
msgstr "Listos para comezar"

#: gnome-initial-setup/pages/summary/gis-summary-page.ui:64
msgid "You’re ready to go!"
msgstr "Todo está listo para comezar"

#. Translators: "city, country"
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:236
#, c-format
msgctxt "timezone loc"
msgid "%s, %s"
msgstr "%s, %s"

#. Translators: UTC here means the Coordinated Universal Time.
#. * %:::z will be replaced by the offset from UTC e.g. UTC+02
#.
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:273
msgid "UTC%:::z"
msgstr "UTC%:::z"

#. Translators: This is the time format used in 12-hour mode.
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:277
msgid "%l:%M %p"
msgstr "%l:%M %p"

#. Translators: This is the time format used in 24-hour mode.
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:280
msgid "%R"
msgstr "%R"

#. Translators: "timezone (utc shift)"
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:283
#, c-format
msgctxt "timezone map"
msgid "%s (%s)"
msgstr "%s (%s)"

#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:426
#: gnome-initial-setup/pages/timezone/gis-timezone-page.ui:33
msgid "Time Zone"
msgstr "Fuso horario"

#: gnome-initial-setup/pages/timezone/gis-timezone-page.ui:53
msgid ""
"The time zone will be set automatically if your location can be found. You "
"can also search for a city to set it yourself."
msgstr ""
"O fuso horario configurarase automaticamente se se puido atopar a súa "
"localización. Tamén pode buscar unha cidade vostede mesmo."

#: gnome-initial-setup/pages/timezone/gis-timezone-page.ui:82
msgid "Please search for a nearby city"
msgstr "Busque pola cidade máis cercana"

#~ msgid "Disable image"
#~ msgstr "Desactivar imaxe"

#~ msgctxt "Password hint"
#~ msgid "Try to avoid common words."
#~ msgstr "Tente evitar palabras comúns."

#~ msgctxt "Password hint"
#~ msgid "Try to use more numbers."
#~ msgstr "Tente usar máis números."

#~ msgctxt "Password hint"
#~ msgid ""
#~ "Password needs to be longer. Try to add more letters, numbers and "
#~ "punctuation."
#~ msgstr "Tente usar unha mistura de letras, números e puntuación."

#~ msgid ""
#~ "Proprietary software sources provide access to additional software, "
#~ "including web browsers and games. This software typically has "
#~ "restrictions on use and access to source code, and is not provided by %s."
#~ msgstr ""
#~ "Os orixes de software privativo fornecen acceso a software adicional, "
#~ "incluíndo navegadores web e xogos. Este software normalmente ten "
#~ "restricións de uso e acceso ao seu código fonte, e non é fornecido por %s."

#~ msgid "Proprietary Software Sources"
#~ msgstr "Orixes de software propietario"

#~ msgid "A user with the username ‘%s’ already exists."
#~ msgstr "Xa existe un usuario co nome de usuario «%s»."

#~ msgid "_Verify"
#~ msgstr "_Comprobar"

#~ msgctxt "Password strength"
#~ msgid "Strength: Weak"
#~ msgstr "Forza: Débil"

#~ msgctxt "Password strength"
#~ msgid "Strength: Low"
#~ msgstr "Forza: Baixa"

#~ msgctxt "Password strength"
#~ msgid "Strength: Medium"
#~ msgstr "Forza: Media"

#~ msgctxt "Password strength"
#~ msgid "Strength: Good"
#~ msgstr "Forza: Boa"

#~ msgctxt "Password strength"
#~ msgid "Strength: High"
#~ msgstr "Forza: Alta"

#~ msgid "Are these the right details? You can change them if you want."
#~ msgstr "Esta información é correcta? Pode cambiala se quere."

#~ msgid "You can review your online accounts (and add others) after setup."
#~ msgstr ""
#~ "Pode revisar as súas contas en liña (e engadir outras) despois da "
#~ "configuración."

#~ msgid ""
#~ "Thank you for choosing %s.\n"
#~ "We hope that you love it."
#~ msgstr ""
#~ "Grazas por escoller %s.\n"
#~ "Agardamos que o desfrute."

#~ msgid ""
#~ "We think that your time zone is %s. Press Next to continue or search for "
#~ "a city to manually set the time zone."
#~ msgstr ""
#~ "Cremos que o seu fuso horario é %s. Prema seguinte para continuar ou "
#~ "busque a súa cidade manualmente para estabelecer o fuso horario."

#~ msgid "Cancel"
#~ msgstr "Cancelar"

#~ msgid "Your username cannot be changed after setup."
#~ msgstr "O seu nome e usuario non pode cambiarse despois da configuración."

#~ msgid "No password"
#~ msgstr "Sen contrasinal"

#~ msgctxt "Password strength"
#~ msgid "Too short"
#~ msgstr "Demasiado curto"

#~ msgctxt "Password strength"
#~ msgid "Not good enough"
#~ msgstr "Non é bo dabondo"

#~ msgctxt "Password strength"
#~ msgid "Weak"
#~ msgstr "Débil"

#~ msgctxt "Password strength"
#~ msgid "Fair"
#~ msgstr "Aceptábel"

#~ msgctxt "Password strength"
#~ msgid "Good"
#~ msgstr "Bo"

#~ msgctxt "Password strength"
#~ msgid "Strong"
#~ msgstr "Forte"

#~ msgid "Login"
#~ msgstr "Chave de acceso"

#~ msgid "Create a Local Account"
#~ msgstr "Crear unha conta local"

#~ msgid "page 1"
#~ msgstr "páxina 1"

#~ msgid "Create an Enterprise Account"
#~ msgstr "Crear unha conta corporativa"

#~ msgid "Other"
#~ msgstr "Outra"

#~ msgid "No input source selected"
#~ msgstr "Non se seleccionou ningunha orixe de entrada"

#~ msgid "Add an Input Source"
#~ msgstr "Engadir unha orixe de entrada"

#~ msgctxt "Online Account"
#~ msgid "Other"
#~ msgstr "Outra"

#~ msgid "Mail"
#~ msgstr "Correo electrónico"

#~ msgid "Contacts"
#~ msgstr "Contactos"

#~ msgid "Chat"
#~ msgstr "Conversa"

#~ msgid "Resources"
#~ msgstr "Recursos"

#~ msgid "Error creating account"
#~ msgstr "Produciuse un erro ao crear a conta"

#~ msgid "Error removing account"
#~ msgstr "Produciuse un erro ao retirar a conta"

#~ msgid "Are you sure you want to remove the account?"
#~ msgstr "Ten certeza que quere retirar a conta?"

#~ msgid "This will not remove the account on the server."
#~ msgstr "Isto non retirará a conta do servidor."

#~ msgid "_Remove"
#~ msgstr "_Retirar"

#~ msgid "Connect to your existing data in the cloud"
#~ msgstr "Conectarse aos seus datos existentes na nube"

#~ msgid "_Add Account"
#~ msgstr "_Engadir conta"

#~ msgid "Select keyboard layouts"
#~ msgstr "Seleccionar disposicións de teclado"

#~ msgid "Login settings are used by all users when logging into the system"
#~ msgstr ""
#~ "As preferencias de inicio de sesión úsanas todos os usuarios ao iniciar a "
#~ "sesión no sistema"

#~ msgid "Search for a location"
#~ msgstr "Buscar por unha localización"

#~ msgid "_Determine your location automatically"
#~ msgstr "_Determinar a súa localización automaticamente"

#~ msgid "No network devices found."
#~ msgstr "Non foi posíbel atopar ningún dispositivo de rede."

#~ msgid "Thank You"
#~ msgstr "Grazas"

#~ msgid "Your computer is ready to use."
#~ msgstr "O seu computador está listo para o seu uso."

#~ msgid "You may change these options at any time in Settings."
#~ msgstr "Pode cambiar estas opcións en calquera momento desde Preferencias."

#~ msgid "_Start using GNOME 3"
#~ msgstr "_Comezar a usar GNOME 3"

#~ msgid "Select input sources"
#~ msgstr "Seleccionar a orixe de entrada"

#~ msgid "_Back"
#~ msgstr "_Anterior"

#~ msgid "Sorry"
#~ msgstr "Desculpe"

#~ msgid "Input methods can't be used on the login screen"
#~ msgstr ""
#~ "Os métodos de entrada non poden usarse na pantalla de inicio de sesión"

#~ msgid "Your session needs to be restarted for changes to take effect"
#~ msgstr "Debe reiniciar a súa sesión para que se apliquen os cambios"

#~ msgid "Restart Now"
#~ msgstr "Reiniciar agora"

#~ msgctxt "Language"
#~ msgid "None"
#~ msgstr "Ningún"

#~ msgid "Select an input source"
#~ msgstr "Seleccione unha orixe de entrada"

#~ msgid "Unspecified"
#~ msgstr "Non especificado"

#~ msgid "_Use Local Login"
#~ msgstr "_Usar una conta local"

#~ msgid "Keyboard Layout"
#~ msgstr "Disposición do teclado"

#~ msgid "Remove Input Source"
#~ msgstr "Retirar orixe de entrada"

#~ msgid "Move Input Source Up"
#~ msgstr "Mover orixe de entrada arriba"

#~ msgid "English"
#~ msgstr "Inglés"

#~ msgid "British English"
#~ msgstr "Inglés británico"

#~ msgid "German"
#~ msgstr "Alemán"

#~ msgid "French"
#~ msgstr "Francés"

#~ msgid "Spanish"
#~ msgstr "Español"

#~ msgid "Chinese (simplified)"
#~ msgstr "Chinés (Simplificado)"

#~ msgid "Use %s"
#~ msgstr "Usar %s"

#~ msgid "_Login Name"
#~ msgstr "Nome de _usuario"

#~ msgid "Link other accounts"
#~ msgstr "Ligar outras contas"

#~ msgid "Choose How to Login"
#~ msgstr "Seleccione como iniciar sesión"

#~ msgid "Remove"
#~ msgstr "Retirar"

#~ msgid "Show _all"
#~ msgstr "Mostrar _todos"

#~ msgid "Enjoy GNOME!"
#~ msgstr "Goce de GNOME!"

#~ msgid "New to GNOME 3 and need help finding your way around?"
#~ msgstr "É novo en GNOME 3 e precisa axuda para atopar o seu camiño?"

#~ msgid "_Take a Tour"
#~ msgstr "_Facer unha visita guiada"

#~ msgid "_Require a password to use this account"
#~ msgstr "_Requírese un contrasinal para usar esta conta"

#~ msgid "_Act as administrator of this computer"
#~ msgstr "_Actuar como administrador deste computador"

#~ msgid "_Done"
#~ msgstr "_Feito"
